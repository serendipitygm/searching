/*
 Navicat MySQL Data Transfer

 Source Server         : local
 Source Server Type    : MySQL
 Source Server Version : 50641
 Source Host           : localhost:3306
 Source Schema         : st_searching

 Target Server Type    : MySQL
 Target Server Version : 50641
 File Encoding         : 65001

 Date: 30/11/2019 10:30:34
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for st_feedback_info
-- ----------------------------
DROP TABLE IF EXISTS `st_feedback_info`;
CREATE TABLE `st_feedback_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Primary_key',
  `feedback_date` date DEFAULT NULL COMMENT '反馈日期',
  `feedback_company` varchar(255) DEFAULT NULL COMMENT '反馈公司',
  `feedback_person` varchar(255) DEFAULT NULL COMMENT '反馈人',
  `reason` varchar(255) DEFAULT NULL COMMENT '产生原因',
  `issue` varchar(255) DEFAULT NULL COMMENT '问题点',
  `solution` varchar(255) DEFAULT NULL COMMENT '解决方式',
  `issues_category` varchar(255) DEFAULT NULL COMMENT '问题分类',
  `remarks` varchar(255) DEFAULT NULL COMMENT '备注',
  `processor` varchar(255) DEFAULT NULL COMMENT '处理人',
  `pictures` varchar(255) DEFAULT NULL,
  `image` longblob,
  `photo` longblob,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=66 DEFAULT CHARSET=utf8;

SET FOREIGN_KEY_CHECKS = 1;
