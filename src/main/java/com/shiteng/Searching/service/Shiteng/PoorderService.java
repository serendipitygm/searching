package com.shiteng.Searching.service.Shiteng;

import com.shiteng.Searching.pojo.Poorder_ms;

import java.util.List;

/**
 * Created by Fang on 2020-01-06 10:20
 */
public interface PoorderService {
    List<Poorder_ms> findAllByPoorder();
}
