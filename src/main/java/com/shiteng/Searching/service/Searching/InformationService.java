package com.shiteng.Searching.service.Searching;

import com.shiteng.Searching.pojo.FeedbackInfo;
import com.shiteng.Searching.pojo.Poorder;
import com.shiteng.Searching.pojo.Poorder_ms;

import java.util.List;

/**
 * Created by Fang on 18/3/14.
 */

public interface InformationService {

    List<FeedbackInfo> findAll();

    List<FeedbackInfo> findAllByIssueLike(String issue);

    FeedbackInfo saveInfo(FeedbackInfo feedbackInfo);

    List<FeedbackInfo> findAllById(Integer id);

}
