package com.shiteng.Searching.service;

import com.shiteng.Searching.dao.InformationDao;
import com.shiteng.Searching.pojo.FeedbackInfo;
import com.shiteng.Searching.pojo.Poorder_ms;
import com.shiteng.Searching.service.Searching.InformationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Fang on 18/3/14.
 */
@Service
public class InformationServiceImpl implements InformationService {

//    @Autowired
    private InformationDao informationDao;

    @Autowired
    public InformationServiceImpl(InformationDao informationDao) {
        this.informationDao = informationDao;
    }

    @Override
    public List<FeedbackInfo> findAll() {
        return informationDao.findAll();
    }

    @Override
    public List<FeedbackInfo> findAllByIssueLike(String issue) {
        return informationDao.findAllByIssueLike(issue);
    }

    @Override
    public FeedbackInfo saveInfo(FeedbackInfo feedbackInfo) {
        return informationDao.save(feedbackInfo);
    }

    @Override
    public List<FeedbackInfo> findAllById(Integer id) {
        return informationDao.findAllById(id);
    }


}
