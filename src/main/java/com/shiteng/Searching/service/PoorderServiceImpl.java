package com.shiteng.Searching.service;

import com.shiteng.Searching.dao.InformationDao;
import com.shiteng.Searching.pojo.Poorder_ms;
import com.shiteng.Searching.service.Shiteng.PoorderService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * Created by Fang on 2020-01-06 10:21
 */
public class PoorderServiceImpl implements PoorderService {

    public PoorderServiceImpl(InformationDao informationDao) {
        this.informationDao = informationDao;
    }

    @Autowired
    private InformationDao informationDao;

    @Override
    public List<Poorder_ms> findAllByPoorder() {
        return informationDao.findAllByPoorder();
    }
}
