package com.shiteng.Searching.pojo;

import javax.persistence.*;

/**
 * Created by Fang on 2019-12-06 11:36
 */
@Entity
@Table(name = "statistics")
public class Statistics {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(name = "companyName")
    private String companyName;
    @Column(name = "purchaseNineteen")
    private String purchaseNineteen;
    @Column(name = "estimated")
    private String estimated;
    @Column(name = "purchaseEighteen")
    private String purchaseEighteen;
    @Column(name = "growthRatio")
    private String growthRatio;
    @Column(name = "productName")
    private String productName;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getPurchaseNineteen() {
        return purchaseNineteen;
    }

    public void setPurchaseNineteen(String purchaseNineteen) {
        this.purchaseNineteen = purchaseNineteen;
    }

    public String getEstimated() {
        return estimated;
    }

    public void setEstimated(String estimated) {
        this.estimated = estimated;
    }

    public String getPurchaseEighteen() {
        return purchaseEighteen;
    }

    public void setPurchaseEighteen(String purchaseEighteen) {
        this.purchaseEighteen = purchaseEighteen;
    }

    public String getGrowthRatio() {
        return growthRatio;
    }

    public void setGrowthRatio(String growthRatio) {
        this.growthRatio = growthRatio;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    @Override
    public String toString() {
        return "Statistics{" +
                "id=" + id +
                ", companyName='" + companyName + '\'' +
                ", purchaseNineteen='" + purchaseNineteen + '\'' +
                ", estimated='" + estimated + '\'' +
                ", purchaseEighteen='" + purchaseEighteen + '\'' +
                ", growthRatio='" + growthRatio + '\'' +
                ", productName='" + productName + '\'' +
                '}';
    }
}
