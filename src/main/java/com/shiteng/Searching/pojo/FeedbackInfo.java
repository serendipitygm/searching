package com.shiteng.Searching.pojo;

import javax.persistence.*;
import java.sql.Blob;

/**
 * Created by Fang on 2019-11-12 14:05
 */
@Entity
@Table(name = "st_feedback_info")
public class FeedbackInfo {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Integer id;
    @Lob
    protected byte[] photo; // 数组类型
    @Lob
    protected java.sql.Blob image; // 大数据类型
    @Column(name = "feedbackDate")
    private String feedbackDate;
    @Column(name = "feedbackCompany")
    private String feedbackCompany;
    @Column(name = "feedbackPerson")
    private String feedbackPerson;
    @Column(name = "reason")
    private String reason;
    @Column(name = "pictures")
    private String pictures;
    @Column(name = "issue")
    private String issue;
    @Column(name = "solution")
    private String solution;
    @Column(name = "issuesCategory")
    private String issuesCategory;
    @Column(name = "processor")
    private String processor;
    @Column(name = "remarks")
    private String remarks;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFeedbackDate() {
        return feedbackDate;
    }

    public void setFeedbackDate(String feedbackDate) {
        this.feedbackDate = feedbackDate;
    }

    public String getFeedbackCompany() {
        return feedbackCompany;
    }

    public void setFeedbackCompany(String feedbackCompany) {
        this.feedbackCompany = feedbackCompany;
    }

    public String getFeedbackPerson() {
        return feedbackPerson;
    }

    public void setFeedbackPerson(String feedbackPerson) {
        this.feedbackPerson = feedbackPerson;
    }

    public String getIssue() {
        return issue;
    }

    public void setIssue(String issue) {
        this.issue = issue;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getPictures() {
        return pictures;
    }

    public void setPictures(String pictures) {
        this.pictures = pictures;
    }

    public String getSolution() {
        return solution;
    }

    public void setSolution(String solution) {
        this.solution = solution;
    }

    public String getIssuesCategory() {
        return issuesCategory;
    }

    public void setIssuesCategory(String issuesCategory) {
        this.issuesCategory = issuesCategory;
    }

    public String getProcessor() {
        return processor;
    }

    public void setProcessor(String processor) {
        this.processor = processor;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public byte[] getPhoto() {
        return photo;
    }

    public void setPhoto(byte[] photo) {
        this.photo = photo;
    }

    public Blob getImage() {
        return image;
    }

    public void setImage(Blob image) {
        this.image = image;
    }

    @Override
    public String toString() {
        return "FeedbackInfo{" +
                "id=" + id +
                ", feedbackDate='" + feedbackDate + '\'' +
                ", feedbackCompany='" + feedbackCompany + '\'' +
                ", feedbackPerson='" + feedbackPerson + '\'' +
                ", issue='" + issue + '\'' +
                ", solution='" + solution + '\'' +
                ", issuesCategory='" + issuesCategory + '\'' +
                ", processor='" + processor + '\'' +
                ", remarks='" + remarks + '\'' +
                '}';
    }
}
