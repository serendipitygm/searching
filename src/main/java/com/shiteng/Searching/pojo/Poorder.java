package com.shiteng.Searching.pojo;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import javax.persistence.*;

/**
 * Created by Fang on 2019-12-25 15:51
 */
@Entity
@Table(name = "T_PUR_POORDER")
@ConfigurationProperties(prefix = "spring.datasource.druid.secondly.AIS_K3CLOUD_V7_SHITENG")
@Component
@Data
public class Poorder {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Integer fid;
    @Column(name = "FBILLTYPEID")
    private String fbilltypeid;
    @Column(name = "FBILLNO")
    private String fbillno;
    @Column(name = "FDATE")
    private String fdate;
    @Column(name = "FSUPPLIERID")
    private Integer fsupplierid;
    @Column(name = "FPURCHASEORGID")
    private Integer fpurchaseorgid;
    @Column(name = "FPURCHASERGROUPID")
    private Integer fpurchasegroupid;
    @Column(name = "FPURCHASEDEPTID")
    private Integer fpurchasedeptid;
    @Column(name = "FPURCHASERID")
    private Integer fpurchaserid;
    @Column(name = "FCREATORID")
    private Integer fcreatorid;
    @Column(name = "FCREATEDATE")
    private String fcreatedate;
    @Column(name = "FMODIFIERID")
    private Integer fmodifierid;
    @Column(name = "FMODIFYDATE")
    private String fmodifydate;
    @Column(name = "FDOCUMENTSTATUS")
    private String fdocumentstatus;
    @Column(name = "FAPPROVERID")
    private Integer fapproverid;
    @Column(name = "FAPPROVEDATE")
    private String fapprovedate;
    @Column(name = "FCLOSESTATUS")
    private String fclosestatus;
    @Column(name = "FCLOSERID")
    private Integer fcloserid;
    @Column(name = "FCLOSEDATE")
    private String fclosedate;
    @Column(name = "FCANCELSTATUS")
    private String fcancelstatus;
    @Column(name = "FCANCELLERID")
    private Integer fcancellerid;
    @Column(name = "FCANCELDATE")
    private String fcanceldate;
    @Column(name = "FPROVIDERID")
    private Integer fproviderid;
    @Column(name = "FSETTLEID")
    private Integer fsettleid;
    @Column(name = "FCHARGEID")
    private Integer fchargeid;
    @Column(name = "FVERSIONNO")
    private String fversionno;
    @Column(name = "FCHANGEREASON")
    private String fchangereason;
    @Column(name = "FCHANGEDATE")
    private String fchangedate;
    @Column(name = "FCHANGERID")
    private Integer fchangerid;
    @Column(name = "FISCONVERT")
    private String flsconvert;
    @Column(name = "FPURCATALOGID")
    private Integer fpurcatalogin;
    @Column(name = "FBUSINESSTYPE")
    private String fbusinesstype;
    @Column(name = "FPROVIDERADDRESS")
    private String fprovideraddress;
    @Column(name = "FOBJECTTYPEID")
    private String fobjecttypeid;
    @Column(name = "FASSIGNSUPPLIERID")
    private Integer fassignsupplierid;
    @Column(name = "FCORRESPONDORGID")
    private Integer fcorrespondorgid;
    @Column(name = "FPROVIDERCONTACT")
    private String fprovidercontact;
    @Column(name = "FNETORDERBILLNO")
    private String fnetorderbillno;
    @Column(name = "FNETORDERBILLID")
    private Integer fnetorderbillid;
    @Column(name = "FCONFIRMSTATUS")
    private String fconfirmstatus;
    @Column(name = "FCONFIRMERID")
    private Integer fconfirmerid;
    @Column(name = "FCONFIRMDATE")
    private String fconfirmdate;
    @Column(name = "FPROVIDERCONTACTID")
    private Integer fprovidercontactid;
    @Column(name = "FBILLTYPEIDVM")
    private String FBILLTYPEIDVM;
    @Column(name = "F_PAEZ_ATTACHMENTCOUNT")
    private Integer F_PAEZ_ATTACHMENTCOUNT;
    @Column(name = "F_PAEZ_COMBO")
    private String F_PAEZ_COMBO;
    @Column(name = "F_PAEZ_COMBO1")
    private String F_PAEZ_COMBO1;
    @Column(name = "F_PAEZ_COMBO2")
    private String F_PAEZ_COMBO2;
    @Column(name = "F_PAEZ_COMBO3")
    private String F_PAEZ_COMBO3;
    @Column(name = "F_PAEZ_COMBO4")
    private String F_PAEZ_COMBO4;
    @Column(name = "F_PAEZ_COMBO5")
    private String F_PAEZ_COMBO5;
    @Column(name = "F_PAEZ_COMBO6")
    private String F_PAEZ_COMBO6;
    @Column(name = "F_PAEZ_COMBO7")
    private String F_PAEZ_COMBO7;
    @Column(name = "F_PAEZ_BASE")
    private Integer F_PAEZ_BASE;
    @Column(name = "F_PAEZ_TEXT")
    private String F_PAEZ_TEXT;
    @Column(name = "F_PAEZ_REMARKS")
    private String F_PAEZ_REMARKS;
    @Column(name = "F_PAEZ_REMARKS1")
    private String F_PAEZ_REMARKS1;
    @Column(name = "F_PAEZ_TEXT1")
    private String F_PAEZ_TEXT1;
    @Column(name = "F_PAEZ_BASE1")
    private Integer F_PAEZ_BASE1;
    @Column(name = "F_PAEZ_BASE2")
    private Integer F_PAEZ_BASE2;
    @Column(name = "F_PAEZ_TEXT2")
    private String F_PAEZ_TEXT2;
    @Column(name = "F_PAEZ_TEXT3")
    private String F_PAEZ_TEXT3;
    @Column(name = "F_PAEZ_REMARKS2")
    private String F_PAEZ_REMARKS2;
    @Column(name = "F_PAEZ_REMARKS3")
    private String F_PAEZ_REMARKS3;
    @Column(name = "F_PAEZ_AMOUNT")
    private Float F_PAEZ_AMOUNT;
    @Column(name = "F_PAEZ_CHECKBOX")
    private String F_PAEZ_CHECKBOX;
    @Column(name = "F_PAEZ_AMOUNT1")
    private Float F_PAEZ_AMOUNT1;
    @Column(name = "F_ESY_COMBO")
    private String F_ESY_COMBO;
    @Column(name = "F_ESY_AMOUNT")
    private Float F_ESY_AMOUNT;
    @Column(name = "F_ESY_TEXT")
    private String F_ESY_TEXT;
    @Column(name = "F_ESY_TEXT1")
    private String F_ESY_TEXT1;
    @Column(name = "F_ESY_BASE")
    private Integer F_ESY_BASE;
    @Column(name = "F_ESY_AMOUNT1")
    private Float F_ESY_AMOUNT1;
    @Column(name = "FSOURCEBILLNO")
    private String FSOURCEBILLNO;
    @Column(name = "F_ESY_AMOUNT2")
    private Float F_ESY_AMOUNT2;
    @Column(name = "F_ESY_AMOUNT4")
    private Float F_ESY_AMOUNT4;
    @Column(name = "F_ESY_COMBO1")
    private String F_ESY_COMBO1;
    @Column(name = "F_ESY_DATE")
    private String F_ESY_DATE;
    @Column(name = "F_ESY_USERID")
    private Integer F_ESY_USERID;
    @Column(name = "F_PAEZ_INTEGER")
    private Integer F_PAEZ_INTEGER;
    @Column(name = "F_PAEZ_BASE3")
    private Integer F_PAEZ_BASE3;
    @Column(name = "F_PAEZ_ATTACHMENT")
    private String F_PAEZ_ATTACHMENT;
    @Column(name = "F_PAEZ_IMAGEFILESERVER")
    private String F_PAEZ_IMAGEFILESERVER;
    @Column(name = "F_PAEZ_XTKZ")
    private Integer F_PAEZ_XTKZ;

    public Integer getFid() {
        return fid;
    }

    public void setFid(Integer fid) {
        this.fid = fid;
    }

    public String getFbilltypeid() {
        return fbilltypeid;
    }

    public void setFbilltypeid(String fbilltypeid) {
        this.fbilltypeid = fbilltypeid;
    }

    public String getFbillno() {
        return fbillno;
    }

    public void setFbillno(String fbillno) {
        this.fbillno = fbillno;
    }

    public String getFdate() {
        return fdate;
    }

    public void setFdate(String fdate) {
        this.fdate = fdate;
    }

    public Integer getFsupplierid() {
        return fsupplierid;
    }

    public void setFsupplierid(Integer fsupplierid) {
        this.fsupplierid = fsupplierid;
    }

    public Integer getFpurchaseorgid() {
        return fpurchaseorgid;
    }

    public void setFpurchaseorgid(Integer fpurchaseorgid) {
        this.fpurchaseorgid = fpurchaseorgid;
    }

    public Integer getFpurchasegroupid() {
        return fpurchasegroupid;
    }

    public void setFpurchasegroupid(Integer fpurchasegroupid) {
        this.fpurchasegroupid = fpurchasegroupid;
    }

    public Integer getFpurchasedeptid() {
        return fpurchasedeptid;
    }

    public void setFpurchasedeptid(Integer fpurchasedeptid) {
        this.fpurchasedeptid = fpurchasedeptid;
    }

    public Integer getFpurchaserid() {
        return fpurchaserid;
    }

    public void setFpurchaserid(Integer fpurchaserid) {
        this.fpurchaserid = fpurchaserid;
    }

    public Integer getFcreatorid() {
        return fcreatorid;
    }

    public void setFcreatorid(Integer fcreatorid) {
        this.fcreatorid = fcreatorid;
    }

    public String getFcreatedate() {
        return fcreatedate;
    }

    public void setFcreatedate(String fcreatedate) {
        this.fcreatedate = fcreatedate;
    }

    public Integer getFmodifierid() {
        return fmodifierid;
    }

    public void setFmodifierid(Integer fmodifierid) {
        this.fmodifierid = fmodifierid;
    }

    public String getFmodifydate() {
        return fmodifydate;
    }

    public void setFmodifydate(String fmodifydate) {
        this.fmodifydate = fmodifydate;
    }

    public String getFdocumentstatus() {
        return fdocumentstatus;
    }

    public void setFdocumentstatus(String fdocumentstatus) {
        this.fdocumentstatus = fdocumentstatus;
    }

    public Integer getFapproverid() {
        return fapproverid;
    }

    public void setFapproverid(Integer fapproverid) {
        this.fapproverid = fapproverid;
    }

    public String getFapprovedate() {
        return fapprovedate;
    }

    public void setFapprovedate(String fapprovedate) {
        this.fapprovedate = fapprovedate;
    }

    public String getFclosestatus() {
        return fclosestatus;
    }

    public void setFclosestatus(String fclosestatus) {
        this.fclosestatus = fclosestatus;
    }

    public Integer getFcloserid() {
        return fcloserid;
    }

    public void setFcloserid(Integer fcloserid) {
        this.fcloserid = fcloserid;
    }

    public String getFclosedate() {
        return fclosedate;
    }

    public void setFclosedate(String fclosedate) {
        this.fclosedate = fclosedate;
    }

    public String getFcancelstatus() {
        return fcancelstatus;
    }

    public void setFcancelstatus(String fcancelstatus) {
        this.fcancelstatus = fcancelstatus;
    }

    public Integer getFcancellerid() {
        return fcancellerid;
    }

    public void setFcancellerid(Integer fcancellerid) {
        this.fcancellerid = fcancellerid;
    }

    public String getFcanceldate() {
        return fcanceldate;
    }

    public void setFcanceldate(String fcanceldate) {
        this.fcanceldate = fcanceldate;
    }

    public Integer getFproviderid() {
        return fproviderid;
    }

    public void setFproviderid(Integer fproviderid) {
        this.fproviderid = fproviderid;
    }

    public Integer getFsettleid() {
        return fsettleid;
    }

    public void setFsettleid(Integer fsettleid) {
        this.fsettleid = fsettleid;
    }

    public Integer getFchargeid() {
        return fchargeid;
    }

    public void setFchargeid(Integer fchargeid) {
        this.fchargeid = fchargeid;
    }

    public String getFversionno() {
        return fversionno;
    }

    public void setFversionno(String fversionno) {
        this.fversionno = fversionno;
    }

    public String getFchangereason() {
        return fchangereason;
    }

    public void setFchangereason(String fchangereason) {
        this.fchangereason = fchangereason;
    }

    public String getFchangedate() {
        return fchangedate;
    }

    public void setFchangedate(String fchangedate) {
        this.fchangedate = fchangedate;
    }

    public Integer getFchangerid() {
        return fchangerid;
    }

    public void setFchangerid(Integer fchangerid) {
        this.fchangerid = fchangerid;
    }

    public String getFlsconvert() {
        return flsconvert;
    }

    public void setFlsconvert(String flsconvert) {
        this.flsconvert = flsconvert;
    }

    public Integer getFpurcatalogin() {
        return fpurcatalogin;
    }

    public void setFpurcatalogin(Integer fpurcatalogin) {
        this.fpurcatalogin = fpurcatalogin;
    }

    public String getFbusinesstype() {
        return fbusinesstype;
    }

    public void setFbusinesstype(String fbusinesstype) {
        this.fbusinesstype = fbusinesstype;
    }

    public String getFprovideraddress() {
        return fprovideraddress;
    }

    public void setFprovideraddress(String fprovideraddress) {
        this.fprovideraddress = fprovideraddress;
    }

    public String getFobjecttypeid() {
        return fobjecttypeid;
    }

    public void setFobjecttypeid(String fobjecttypeid) {
        this.fobjecttypeid = fobjecttypeid;
    }

    public Integer getFassignsupplierid() {
        return fassignsupplierid;
    }

    public void setFassignsupplierid(Integer fassignsupplierid) {
        this.fassignsupplierid = fassignsupplierid;
    }

    public Integer getFcorrespondorgid() {
        return fcorrespondorgid;
    }

    public void setFcorrespondorgid(Integer fcorrespondorgid) {
        this.fcorrespondorgid = fcorrespondorgid;
    }

    public String getFprovidercontact() {
        return fprovidercontact;
    }

    public void setFprovidercontact(String fprovidercontact) {
        this.fprovidercontact = fprovidercontact;
    }

    public String getFnetorderbillno() {
        return fnetorderbillno;
    }

    public void setFnetorderbillno(String fnetorderbillno) {
        this.fnetorderbillno = fnetorderbillno;
    }

    public Integer getFnetorderbillid() {
        return fnetorderbillid;
    }

    public void setFnetorderbillid(Integer fnetorderbillid) {
        this.fnetorderbillid = fnetorderbillid;
    }

    public String getFconfirmstatus() {
        return fconfirmstatus;
    }

    public void setFconfirmstatus(String fconfirmstatus) {
        this.fconfirmstatus = fconfirmstatus;
    }

    public Integer getFconfirmerid() {
        return fconfirmerid;
    }

    public void setFconfirmerid(Integer fconfirmerid) {
        this.fconfirmerid = fconfirmerid;
    }

    public String getFconfirmdate() {
        return fconfirmdate;
    }

    public void setFconfirmdate(String fconfirmdate) {
        this.fconfirmdate = fconfirmdate;
    }

    public Integer getFprovidercontactid() {
        return fprovidercontactid;
    }

    public void setFprovidercontactid(Integer fprovidercontactid) {
        this.fprovidercontactid = fprovidercontactid;
    }

    public String getFBILLTYPEIDVM() {
        return FBILLTYPEIDVM;
    }

    public void setFBILLTYPEIDVM(String FBILLTYPEIDVM) {
        this.FBILLTYPEIDVM = FBILLTYPEIDVM;
    }

    public Integer getF_PAEZ_ATTACHMENTCOUNT() {
        return F_PAEZ_ATTACHMENTCOUNT;
    }

    public void setF_PAEZ_ATTACHMENTCOUNT(Integer f_PAEZ_ATTACHMENTCOUNT) {
        F_PAEZ_ATTACHMENTCOUNT = f_PAEZ_ATTACHMENTCOUNT;
    }

    public String getF_PAEZ_COMBO() {
        return F_PAEZ_COMBO;
    }

    public void setF_PAEZ_COMBO(String f_PAEZ_COMBO) {
        F_PAEZ_COMBO = f_PAEZ_COMBO;
    }

    public String getF_PAEZ_COMBO1() {
        return F_PAEZ_COMBO1;
    }

    public void setF_PAEZ_COMBO1(String f_PAEZ_COMBO1) {
        F_PAEZ_COMBO1 = f_PAEZ_COMBO1;
    }

    public String getF_PAEZ_COMBO2() {
        return F_PAEZ_COMBO2;
    }

    public void setF_PAEZ_COMBO2(String f_PAEZ_COMBO2) {
        F_PAEZ_COMBO2 = f_PAEZ_COMBO2;
    }

    public String getF_PAEZ_COMBO3() {
        return F_PAEZ_COMBO3;
    }

    public void setF_PAEZ_COMBO3(String f_PAEZ_COMBO3) {
        F_PAEZ_COMBO3 = f_PAEZ_COMBO3;
    }

    public String getF_PAEZ_COMBO4() {
        return F_PAEZ_COMBO4;
    }

    public void setF_PAEZ_COMBO4(String f_PAEZ_COMBO4) {
        F_PAEZ_COMBO4 = f_PAEZ_COMBO4;
    }

    public String getF_PAEZ_COMBO5() {
        return F_PAEZ_COMBO5;
    }

    public void setF_PAEZ_COMBO5(String f_PAEZ_COMBO5) {
        F_PAEZ_COMBO5 = f_PAEZ_COMBO5;
    }

    public String getF_PAEZ_COMBO6() {
        return F_PAEZ_COMBO6;
    }

    public void setF_PAEZ_COMBO6(String f_PAEZ_COMBO6) {
        F_PAEZ_COMBO6 = f_PAEZ_COMBO6;
    }

    public String getF_PAEZ_COMBO7() {
        return F_PAEZ_COMBO7;
    }

    public void setF_PAEZ_COMBO7(String f_PAEZ_COMBO7) {
        F_PAEZ_COMBO7 = f_PAEZ_COMBO7;
    }

    public Integer getF_PAEZ_BASE() {
        return F_PAEZ_BASE;
    }

    public void setF_PAEZ_BASE(Integer f_PAEZ_BASE) {
        F_PAEZ_BASE = f_PAEZ_BASE;
    }

    public String getF_PAEZ_TEXT() {
        return F_PAEZ_TEXT;
    }

    public void setF_PAEZ_TEXT(String f_PAEZ_TEXT) {
        F_PAEZ_TEXT = f_PAEZ_TEXT;
    }

    public String getF_PAEZ_REMARKS() {
        return F_PAEZ_REMARKS;
    }

    public void setF_PAEZ_REMARKS(String f_PAEZ_REMARKS) {
        F_PAEZ_REMARKS = f_PAEZ_REMARKS;
    }

    public String getF_PAEZ_REMARKS1() {
        return F_PAEZ_REMARKS1;
    }

    public void setF_PAEZ_REMARKS1(String f_PAEZ_REMARKS1) {
        F_PAEZ_REMARKS1 = f_PAEZ_REMARKS1;
    }

    public String getF_PAEZ_TEXT1() {
        return F_PAEZ_TEXT1;
    }

    public void setF_PAEZ_TEXT1(String f_PAEZ_TEXT1) {
        F_PAEZ_TEXT1 = f_PAEZ_TEXT1;
    }

    public Integer getF_PAEZ_BASE1() {
        return F_PAEZ_BASE1;
    }

    public void setF_PAEZ_BASE1(Integer f_PAEZ_BASE1) {
        F_PAEZ_BASE1 = f_PAEZ_BASE1;
    }

    public Integer getF_PAEZ_BASE2() {
        return F_PAEZ_BASE2;
    }

    public void setF_PAEZ_BASE2(Integer f_PAEZ_BASE2) {
        F_PAEZ_BASE2 = f_PAEZ_BASE2;
    }

    public String getF_PAEZ_TEXT2() {
        return F_PAEZ_TEXT2;
    }

    public void setF_PAEZ_TEXT2(String f_PAEZ_TEXT2) {
        F_PAEZ_TEXT2 = f_PAEZ_TEXT2;
    }

    public String getF_PAEZ_TEXT3() {
        return F_PAEZ_TEXT3;
    }

    public void setF_PAEZ_TEXT3(String f_PAEZ_TEXT3) {
        F_PAEZ_TEXT3 = f_PAEZ_TEXT3;
    }

    public String getF_PAEZ_REMARKS2() {
        return F_PAEZ_REMARKS2;
    }

    public void setF_PAEZ_REMARKS2(String f_PAEZ_REMARKS2) {
        F_PAEZ_REMARKS2 = f_PAEZ_REMARKS2;
    }

    public String getF_PAEZ_REMARKS3() {
        return F_PAEZ_REMARKS3;
    }

    public void setF_PAEZ_REMARKS3(String f_PAEZ_REMARKS3) {
        F_PAEZ_REMARKS3 = f_PAEZ_REMARKS3;
    }

    public Float getF_PAEZ_AMOUNT() {
        return F_PAEZ_AMOUNT;
    }

    public void setF_PAEZ_AMOUNT(Float f_PAEZ_AMOUNT) {
        F_PAEZ_AMOUNT = f_PAEZ_AMOUNT;
    }

    public String getF_PAEZ_CHECKBOX() {
        return F_PAEZ_CHECKBOX;
    }

    public void setF_PAEZ_CHECKBOX(String f_PAEZ_CHECKBOX) {
        F_PAEZ_CHECKBOX = f_PAEZ_CHECKBOX;
    }

    public Float getF_PAEZ_AMOUNT1() {
        return F_PAEZ_AMOUNT1;
    }

    public void setF_PAEZ_AMOUNT1(Float f_PAEZ_AMOUNT1) {
        F_PAEZ_AMOUNT1 = f_PAEZ_AMOUNT1;
    }

    public String getF_ESY_COMBO() {
        return F_ESY_COMBO;
    }

    public void setF_ESY_COMBO(String f_ESY_COMBO) {
        F_ESY_COMBO = f_ESY_COMBO;
    }

    public Float getF_ESY_AMOUNT() {
        return F_ESY_AMOUNT;
    }

    public void setF_ESY_AMOUNT(Float f_ESY_AMOUNT) {
        F_ESY_AMOUNT = f_ESY_AMOUNT;
    }

    public String getF_ESY_TEXT() {
        return F_ESY_TEXT;
    }

    public void setF_ESY_TEXT(String f_ESY_TEXT) {
        F_ESY_TEXT = f_ESY_TEXT;
    }

    public String getF_ESY_TEXT1() {
        return F_ESY_TEXT1;
    }

    public void setF_ESY_TEXT1(String f_ESY_TEXT1) {
        F_ESY_TEXT1 = f_ESY_TEXT1;
    }

    public Integer getF_ESY_BASE() {
        return F_ESY_BASE;
    }

    public void setF_ESY_BASE(Integer f_ESY_BASE) {
        F_ESY_BASE = f_ESY_BASE;
    }

    public Float getF_ESY_AMOUNT1() {
        return F_ESY_AMOUNT1;
    }

    public void setF_ESY_AMOUNT1(Float f_ESY_AMOUNT1) {
        F_ESY_AMOUNT1 = f_ESY_AMOUNT1;
    }

    public String getFSOURCEBILLNO() {
        return FSOURCEBILLNO;
    }

    public void setFSOURCEBILLNO(String FSOURCEBILLNO) {
        this.FSOURCEBILLNO = FSOURCEBILLNO;
    }

    public Float getF_ESY_AMOUNT2() {
        return F_ESY_AMOUNT2;
    }

    public void setF_ESY_AMOUNT2(Float f_ESY_AMOUNT2) {
        F_ESY_AMOUNT2 = f_ESY_AMOUNT2;
    }

    public Float getF_ESY_AMOUNT4() {
        return F_ESY_AMOUNT4;
    }

    public void setF_ESY_AMOUNT4(Float f_ESY_AMOUNT4) {
        F_ESY_AMOUNT4 = f_ESY_AMOUNT4;
    }

    public String getF_ESY_COMBO1() {
        return F_ESY_COMBO1;
    }

    public void setF_ESY_COMBO1(String f_ESY_COMBO1) {
        F_ESY_COMBO1 = f_ESY_COMBO1;
    }

    public String getF_ESY_DATE() {
        return F_ESY_DATE;
    }

    public void setF_ESY_DATE(String f_ESY_DATE) {
        F_ESY_DATE = f_ESY_DATE;
    }

    public Integer getF_ESY_USERID() {
        return F_ESY_USERID;
    }

    public void setF_ESY_USERID(Integer f_ESY_USERID) {
        F_ESY_USERID = f_ESY_USERID;
    }

    public Integer getF_PAEZ_INTEGER() {
        return F_PAEZ_INTEGER;
    }

    public void setF_PAEZ_INTEGER(Integer f_PAEZ_INTEGER) {
        F_PAEZ_INTEGER = f_PAEZ_INTEGER;
    }

    public Integer getF_PAEZ_BASE3() {
        return F_PAEZ_BASE3;
    }

    public void setF_PAEZ_BASE3(Integer f_PAEZ_BASE3) {
        F_PAEZ_BASE3 = f_PAEZ_BASE3;
    }

    public String getF_PAEZ_ATTACHMENT() {
        return F_PAEZ_ATTACHMENT;
    }

    public void setF_PAEZ_ATTACHMENT(String f_PAEZ_ATTACHMENT) {
        F_PAEZ_ATTACHMENT = f_PAEZ_ATTACHMENT;
    }

    public String getF_PAEZ_IMAGEFILESERVER() {
        return F_PAEZ_IMAGEFILESERVER;
    }

    public void setF_PAEZ_IMAGEFILESERVER(String f_PAEZ_IMAGEFILESERVER) {
        F_PAEZ_IMAGEFILESERVER = f_PAEZ_IMAGEFILESERVER;
    }

    public Integer getF_PAEZ_XTKZ() {
        return F_PAEZ_XTKZ;
    }

    public void setF_PAEZ_XTKZ(Integer f_PAEZ_XTKZ) {
        F_PAEZ_XTKZ = f_PAEZ_XTKZ;
    }

    @Override
    public String toString() {
        return "Poorder{" +
                "fid=" + fid +
                ", fbilltypeid='" + fbilltypeid + '\'' +
                ", fbillno='" + fbillno + '\'' +
                ", fdate='" + fdate + '\'' +
                ", fsupplierid=" + fsupplierid +
                ", fpurchaseorgid=" + fpurchaseorgid +
                ", fpurchasegroupid=" + fpurchasegroupid +
                ", fpurchasedeptid=" + fpurchasedeptid +
                ", fpurchaserid=" + fpurchaserid +
                ", fcreatorid=" + fcreatorid +
                ", fcreatedate='" + fcreatedate + '\'' +
                ", fmodifierid=" + fmodifierid +
                ", fmodifydate='" + fmodifydate + '\'' +
                ", fdocumentstatus='" + fdocumentstatus + '\'' +
                ", fapproverid=" + fapproverid +
                ", fapprovedate='" + fapprovedate + '\'' +
                ", fclosestatus='" + fclosestatus + '\'' +
                ", fcloserid=" + fcloserid +
                ", fclosedate='" + fclosedate + '\'' +
                ", fcancelstatus='" + fcancelstatus + '\'' +
                ", fcancellerid=" + fcancellerid +
                ", fcanceldate='" + fcanceldate + '\'' +
                ", fproviderid=" + fproviderid +
                ", fsettleid=" + fsettleid +
                ", fchargeid=" + fchargeid +
                ", fversionno='" + fversionno + '\'' +
                ", fchangereason='" + fchangereason + '\'' +
                ", fchangedate='" + fchangedate + '\'' +
                ", fchangerid=" + fchangerid +
                ", flsconvert='" + flsconvert + '\'' +
                ", fpurcatalogin=" + fpurcatalogin +
                ", fbusinesstype='" + fbusinesstype + '\'' +
                ", fprovideraddress='" + fprovideraddress + '\'' +
                ", fobjecttypeid='" + fobjecttypeid + '\'' +
                ", fassignsupplierid=" + fassignsupplierid +
                ", fcorrespondorgid=" + fcorrespondorgid +
                ", fprovidercontact='" + fprovidercontact + '\'' +
                ", fnetorderbillno='" + fnetorderbillno + '\'' +
                ", fnetorderbillid=" + fnetorderbillid +
                ", fconfirmstatus='" + fconfirmstatus + '\'' +
                ", fconfirmerid=" + fconfirmerid +
                ", fconfirmdate='" + fconfirmdate + '\'' +
                ", fprovidercontactid=" + fprovidercontactid +
                ", FBILLTYPEIDVM='" + FBILLTYPEIDVM + '\'' +
                ", F_PAEZ_ATTACHMENTCOUNT=" + F_PAEZ_ATTACHMENTCOUNT +
                ", F_PAEZ_COMBO='" + F_PAEZ_COMBO + '\'' +
                ", F_PAEZ_COMBO1='" + F_PAEZ_COMBO1 + '\'' +
                ", F_PAEZ_COMBO2='" + F_PAEZ_COMBO2 + '\'' +
                ", F_PAEZ_COMBO3='" + F_PAEZ_COMBO3 + '\'' +
                ", F_PAEZ_COMBO4='" + F_PAEZ_COMBO4 + '\'' +
                ", F_PAEZ_COMBO5='" + F_PAEZ_COMBO5 + '\'' +
                ", F_PAEZ_COMBO6='" + F_PAEZ_COMBO6 + '\'' +
                ", F_PAEZ_COMBO7='" + F_PAEZ_COMBO7 + '\'' +
                ", F_PAEZ_BASE=" + F_PAEZ_BASE +
                ", F_PAEZ_TEXT='" + F_PAEZ_TEXT + '\'' +
                ", F_PAEZ_REMARKS='" + F_PAEZ_REMARKS + '\'' +
                ", F_PAEZ_REMARKS1='" + F_PAEZ_REMARKS1 + '\'' +
                ", F_PAEZ_TEXT1='" + F_PAEZ_TEXT1 + '\'' +
                ", F_PAEZ_BASE1=" + F_PAEZ_BASE1 +
                ", F_PAEZ_BASE2=" + F_PAEZ_BASE2 +
                ", F_PAEZ_TEXT2='" + F_PAEZ_TEXT2 + '\'' +
                ", F_PAEZ_TEXT3='" + F_PAEZ_TEXT3 + '\'' +
                ", F_PAEZ_REMARKS2='" + F_PAEZ_REMARKS2 + '\'' +
                ", F_PAEZ_REMARKS3='" + F_PAEZ_REMARKS3 + '\'' +
                ", F_PAEZ_AMOUNT=" + F_PAEZ_AMOUNT +
                ", F_PAEZ_CHECKBOX='" + F_PAEZ_CHECKBOX + '\'' +
                ", F_PAEZ_AMOUNT1=" + F_PAEZ_AMOUNT1 +
                ", F_ESY_COMBO='" + F_ESY_COMBO + '\'' +
                ", F_ESY_AMOUNT=" + F_ESY_AMOUNT +
                ", F_ESY_TEXT='" + F_ESY_TEXT + '\'' +
                ", F_ESY_TEXT1='" + F_ESY_TEXT1 + '\'' +
                ", F_ESY_BASE=" + F_ESY_BASE +
                ", F_ESY_AMOUNT1=" + F_ESY_AMOUNT1 +
                ", FSOURCEBILLNO='" + FSOURCEBILLNO + '\'' +
                ", F_ESY_AMOUNT2=" + F_ESY_AMOUNT2 +
                ", F_ESY_AMOUNT4=" + F_ESY_AMOUNT4 +
                ", F_ESY_COMBO1='" + F_ESY_COMBO1 + '\'' +
                ", F_ESY_DATE='" + F_ESY_DATE + '\'' +
                ", F_ESY_USERID=" + F_ESY_USERID +
                ", F_PAEZ_INTEGER=" + F_PAEZ_INTEGER +
                ", F_PAEZ_BASE3=" + F_PAEZ_BASE3 +
                ", F_PAEZ_ATTACHMENT='" + F_PAEZ_ATTACHMENT + '\'' +
                ", F_PAEZ_IMAGEFILESERVER='" + F_PAEZ_IMAGEFILESERVER + '\'' +
                ", F_PAEZ_XTKZ=" + F_PAEZ_XTKZ +
                '}';
    }
}
