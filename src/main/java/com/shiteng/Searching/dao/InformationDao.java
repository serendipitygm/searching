package com.shiteng.Searching.dao;

import com.shiteng.Searching.pojo.FeedbackInfo;
import com.shiteng.Searching.pojo.Poorder_ms;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public interface InformationDao extends JpaRepository<FeedbackInfo, String> {

    @Query(value = "SELECT * FROM st_feedback_info fb ORDER BY fb.id DESC", nativeQuery = true)
    List<FeedbackInfo> findAll();

    @Query(value = "SELECT * FROM st_feedback_info fb WHERE fb.issue LIKE CONCAT('%',?1,'%')", nativeQuery = true)
    List<FeedbackInfo> findAllByIssueLike(String issue);

    List<FeedbackInfo> findAllById(Integer id);

    //@Query(value = "insert into st_feedback_info values(NULL,?1.feedbackDate,?1.feedbackCompany,?1.feedbackPerson,?1.issue,?1.solution,?1.issuesCategory,?1.processor,?1.remarks)", nativeQuery = true)
    FeedbackInfo save(FeedbackInfo feedbackInfo);
//
//    //@Modifying
//    @Query(value = "select id,companyName,purchaseNineteen,estimated,purchaseEighteen,growthRatio,productName from statistics",nativeQuery = true)
//    List<Statistics> findStatistics();
    @Query(value = "SELECT * FROM Poorder pd",nativeQuery = true)
    List<Poorder_ms> findAllByPoorder();
}
