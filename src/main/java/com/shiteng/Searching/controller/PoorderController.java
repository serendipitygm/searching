package com.shiteng.Searching.controller;

import com.shiteng.Searching.dao.InformationDao;
import com.shiteng.Searching.pojo.FeedbackInfo;
import com.shiteng.Searching.pojo.Poorder;
import com.shiteng.Searching.pojo.Poorder_ms;
import com.shiteng.Searching.service.Searching.InformationService;
import com.shiteng.Searching.service.Shiteng.PoorderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

/**
 * Created by Fang on 2019-12-25 16:12
 */
@Controller
public class PoorderController {
    //    @Autowired(required = false)
//    public PoorderController(PoorderService poorderService, InformationService informationService) {
//        this.poorderService = poorderService;
//        this.informationService = informationService;
//    }
    @Autowired
    private InformationDao poorderService;
//    @Autowired
//    private InformationService informationService;

    @RequestMapping(value = "/queryInformation", produces = {"application/json; charset=UTF-8"},
            method = RequestMethod.GET)
    public ModelAndView jumping() {
//        sql server
        List<Poorder_ms> poorderList = poorderService.findAllByPoorder();
//        mysql
//        List<Poorder_ms> poorderMsList = informationService.findAll();
        return null;
    }
}
