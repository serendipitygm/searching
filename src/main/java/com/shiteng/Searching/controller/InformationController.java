package com.shiteng.Searching.controller;

import com.shiteng.Searching.pojo.FeedbackInfo;
import com.shiteng.Searching.service.Searching.InformationService;
import com.shiteng.Searching.util.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ResourceLoader;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

/**
 * Created by *** on 18/3/14.
 */
//@RestController
@Transactional
@Controller
public class InformationController {

    // TODO hey dude, welcome to the company ^_^,That meaning its your bitter time
    //      Listen, don't try to rewrite this at all， build a new project, and remove to trash
    //      Hahaha, it really sucks,caz I wrote it aimlessly,requirement sucks, code sucks P.M sucks
    //      I even suspected he knew nothing
    @Autowired
    private InformationService informationService;

//    @Autowired
//    public InformationController(InformationService informationService, ResourceLoader resourceLoader) {
//        this.informationService = informationService;
//        this.resourceLoader = resourceLoader;
//    }

    @RequestMapping(value = "/queryInformation", produces = {"application/json; charset=UTF-8"},
            method = RequestMethod.GET)
    public ModelAndView queryEmpAll(HttpServletRequest request) {
        String param = request.getParameter("name");
        List<FeedbackInfo> feedbackInfoList;
        if (StringUtils.isNotBlank(param)) {
            feedbackInfoList = informationService.findAllByIssueLike(param);
        } else {
            feedbackInfoList = informationService.findAll();
        }
        System.out.println(new ModelAndView("index", "feedbackInfoList", feedbackInfoList));
        return new ModelAndView("index", "feedbackInfoList", feedbackInfoList);
    }

    @RequestMapping(value = "/ErRoR_MeT", produces = {"application/json; charset=UTF-8"},
            method = RequestMethod.GET)
    public ModelAndView findAllById(HttpServletRequest error_2) {
        String error_1 = error_2.getParameter("name");
        System.out.println("n_n_name:" + error_1);
        List<FeedbackInfo> error_l = informationService.findAllById(Integer.valueOf(error_1));
        System.out.println(new ModelAndView("infoDetail", "error_l", error_l));
//        Object o =  "["+error_l+"]";
        return new ModelAndView("infoDetail", "error_l", error_l);
    }

    @RequestMapping(value = "/b1vf3hk")
    public String saveInfo(HttpServletRequest request, FeedbackInfo feedbackInfo) {
        feedbackInfo.setFeedbackDate(request.getParameter("filed_1"));
        feedbackInfo.setFeedbackCompany(request.getParameter("filed_2"));
        feedbackInfo.setFeedbackPerson(request.getParameter("filed_3"));
        feedbackInfo.setReason(request.getParameter("filed_4"));
        feedbackInfo.setIssue(request.getParameter("filed_5"));
        feedbackInfo.setSolution(request.getParameter("filed_6"));
        feedbackInfo.setIssuesCategory(request.getParameter("filed_7"));
        feedbackInfo.setProcessor(request.getParameter("filed_8"));
        feedbackInfo.setRemarks(request.getParameter("filed_9"));
        feedbackInfo.setPictures(request.getParameter("filed_10"));
        // TODO: 2019-12-17 改成JSON格式一次性传输
        informationService.saveInfo(feedbackInfo);
        System.out.println("data:" + feedbackInfo);
        return "redirect:/pages/insertInfo.jsp";
//        return informationService.saveInfo(feedbackInfo);
    }

    //    @ResponseBody
//    @RequiresPermissions("xietong:edu:classInfo:import")
//    @RequestMapping(value = "import")
//    public AjaxJson importFile(@RequestParam("file") MultipartFile file, HttpServletResponse response, HttpServletRequest request) {
//        AjaxJson j = new AjaxJson();
//        try {
//            int successNum = 0;
//            int failureNum = 0;
//            StringBuilder failureMsg = new StringBuilder();
//            ImportExcel ei = new ImportExcel(file, 1, 0);
//            List<ClassInfo> list = ei.getDataList(ClassInfo.class);
//            for (ClassInfo classInfo : list){
//                try{
//                    classInfoService.save(classInfo);
//                    successNum++;
//                }catch(ConstraintViolationException ex){
//                    failureNum++;
//                }catch (Exception ex) {
//                    failureNum++;
//                }
//            }
//            if (failureNum>0){
//                failureMsg.insert(0, "，失败 "+failureNum+" 条详情记录。");
//            }
//            j.setMsg( "已成功导入 "+successNum+" 条详情记录"+failureMsg);
//        } catch (Exception e) {
//            j.setSuccess(false);
//            j.setMsg("导入详情失败！失败信息："+e.getMessage());
//        }
//        return j;
//    }
//    @ResponseBody
//    @RequestMapping("/springUpload")
//    public String  fileUpload(@RequestParam("file") CommonsMultipartFile file) throws IOException {
//
//        //用来检测程序运行时间
//        long  startTime=System.currentTimeMillis();
//        System.out.println("fileName："+file.getOriginalFilename());
//
//        try {
//            //获取输出流
//            OutputStream os=new FileOutputStream("/Users/Fang/Desktop/Pictures/Others"+new Date().getTime()+file.getOriginalFilename());
//            //获取输入流 CommonsMultipartFile 中可以直接得到文件的流
//            InputStream is=file.getInputStream();
//            byte[] bts = new byte[1024];
//            //一个一个字节的读取并写入
//            while(is.read(bts)!=-1)
//            {
//                os.write(bts);
//            }
//            os.flush();
//            os.close();
//            is.close();
//
//        } catch (FileNotFoundException e) {
//            // TODO Auto-generated catch block
//            e.printStackTrace();
//        }
//        long  endTime=System.currentTimeMillis();
//        System.out.println("采用流上传的方式的运行时间："+String.valueOf(endTime-startTime)+"ms");
//        return "/success";
//    }
    @Autowired
    private ResourceLoader resourceLoader;

//    @Autowired
//    public TestController(ResourceLoader resourceLoader) {
//        this.resourceLoader = resourceLoader;
//    }

    @Value("/Users/Fang/Desktop/Pictures")
    private String path;

    /**
     * 跳转到文件上传页面
     * @return
     */
//    @RequestMapping("/")
//    public String toUpload(){
//        return "queryInformation";
//    }

    /**
     * @param file 要上传的文件
     * @return
     */
    @RequestMapping(value = "/fileUpload", produces = {"application/json; charset=UTF-8"})
    public String upload(@RequestParam("fileName") MultipartFile file, Map<String, Object> map) {
        if (StringUtils.isNotBlank("fileName")) {
            System.out.println("null");
        } else {
            System.out.println("empty");
        }
        System.out.println("11-22-33");
        // 要上传的目标文件存放路径
        String localPath = "/Users/Fang/Desktop/Pictures";
        // 上传成功或者失败的提示
        String msg = "";

        if (FileUtils.upload(file, localPath, file.getOriginalFilename())) {
            // 上传成功，给出页面提示
            msg = "上传成功！";
        } else {
            msg = "上传失败！";

        }

        // 显示图片
        map.put("msg", msg);
        map.put("fileName", file.getOriginalFilename());

        return "forward:/insertInfo";
    }

    /**
     * 显示单张图片
     *
     * @return
     */
    @RequestMapping("show")
    public ResponseEntity showPhotos(String fileName){

        try {
            // 由于是读取本机的文件，file是一定要加上的， path是在application配置文件中的路径
            return ResponseEntity.ok(resourceLoader.getResource("file:" + path + fileName));
        } catch (Exception e) {
            return ResponseEntity.notFound().build();
        }
    }

    // TODO If you think work ahead is hard,Just relax
    //      I know some nice places in NingBo
    //      like Dr.Oscar is worth to try
    //      good luck
    @RequestMapping(value = "ist", method = RequestMethod.GET)
    public ModelAndView insertInfo() {
        return new ModelAndView("insertInfo");
    }

}
