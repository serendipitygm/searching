<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD//XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<html>
<head>
    <title>世腾-问题详情</title>
</head>
<%--<%=request.getParameter("name")%>--%>
<%@ taglib prefix="_Ore" uri="http://java.sun.com/jsp/jstl/core" %>
<body style="background-color: #d2d7dd;">
    <div style="text-align: left;margin:0 auto 0 0;width: 300px;">
        <h2>${error_l[0].issue}</h2>
        <div>
            <span>${error_l[0].feedbackDate}</span>
            <span>${error_l[0].feedbackCompany}</span>
        </div>
        <div>
            <img src="${error_l[0].pictures}" alt=""/>
        </div>
        <div style="font-size: 20px">${error_l[0].solution}</div>
    </div>
</body>
</html>