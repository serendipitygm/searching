<%@ page language="java" import="java.util.*" pageEncoding="UTF-8" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD//XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<html>
<head>
    <title>世腾-问题列表</title>
    <script type="text/javascript">
        function fileUpload() {
            var form = document.forms[0];
            // form.data = {name: name};
            // form.type = JSON;
            form.action = "<%=basePath%>fileUpload";
            form.method = "POST";
            form.submit();
        }
    </script>
</head>
<%@ taglib prefix="core" uri="http://java.sun.com/jsp/jstl/core" %>
<body>
<form action="fileUpload" method="post" enctype="multipart/form-data">
    <p>选择文件: <input type="file" name="fileName"/></p>
    <p>
        <button onclick="fileUpload()" value="提交"/>
    </p>
</form>
</body>
</html>