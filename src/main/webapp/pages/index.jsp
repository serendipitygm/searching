<%@ page language="java" import="java.util.*" pageEncoding="UTF-8" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD//XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<html>
<head>
    <title>世腾-问题列表</title>
</head>
<style>
    * {
        box-sizing: border-box;
    }

    body {
        margin: 0;
        padding: 0;
        /*background-image: url(/statics/images/Mountain+Above+The+Clouds-1440x900.jpg);
        background-repeat: no-repeat;
        background-size: 100% 100%;
        background-attachment: fixed;
        font-weight: 500;
        font-family: "Microsoft YaHei", "宋体", "Segoe UI", "Lucida Grande", Helvetica, Arial, sans-serif, FreeSans, Arimo;*/
    }

    div.search {
        padding: 10px 0;
    }

    form {
        position: relative;
        width: 100%;
        margin: 0 auto;
    }

    input, button {
        border: none;
        outline: none;
    }

    input {
        width: 60%;
        height: 25px;
        padding-right: 35px;
    }

    button {
        height: 42px;
        width: 42px;
        cursor: pointer;
        position: absolute;
    }

    .bar7 {
        margin-top: 70px;
        border: blue 1px solid
    }

    form {
        height: 42px;
    }

    input {
        width: 250px;
        border-radius: 15px;
        border: 1px solid #324B4E;
        background: #F9F9F9;
        transition: .3s linear;
        float: right;
        margin-top: 5px;
    }

    input:focus {
        /*width: 300px;*/
    }

    button {
        background: none;
        top: -6px;
        right: 100px;
        background-image: url('/statics/images/sousuo.png');
        background-repeat: no-repeat;
        width: 10px;
        height: 10px;
    }

    button:before {
        background-image: url(/statics/images/sousuo2x.png);
        background-repeat: no-repeat;
    }

    #axs {
        /*overflow: hidden;*/
        /*text-overflow: ellipsis;*/
        /*white-space: nowrap;*/
        /*width: 20px;*/

        display: -webkit-box;
        -webkit-box-orient: vertical;
        -webkit-line-clamp: 1;
        overflow: hidden;
    }

    #header {
        /*border: blue 1px solid;*/
        margin: 0 auto 0 1.5%;
        margin-left: 0px;
        background-color: #FFFFFF;
        width: 100%;
        height: 40px;
        /*background-size: 100% 100%;*/
    }

    #logo {
        background-image: url(/statics/images/myit.png);
        background-repeat: no-repeat;
        background-size: 70px 45px;
        width: 100%;
        /*height: 80px;*/
        float: left
    }

    div {
        /*border: green 1px solid;*/
    }
</style>
<%@ taglib prefix="core" uri="http://java.sun.com/jsp/jstl/core" %>
<body style="background-color: #F2F2F2;">
<div style="margin:0 auto 0 0; width: 100%;height: 600px;">
    <div id="header">
        <div id="logo">
            <div style="float: left;width: 78%;">
                <span style="margin-left: 40%;font-size: 20px;;color: #272827;">世腾-问题反馈</span>
            </div>
            <div style="float: left;width: 65px;font-family: 'Microsoft YaHei';font-size: 11px;margin-left: auto;">
                <span>开发:方俊杰</span><br/>
                <span style="color: red">Ver:&nbsp;v.测试</span>
            </div>
        </div>
    </div>
    <div style="background-color: gray;width: 100%;height: 1px;margin-bottom: 5px;margin-left: -10px;"></div>
    <form action="/queryInformation" style="background-color: #FFFFFF;">

        <div style="width: 100%;height: 100%;margin-left: -60px;margin-top: -5px;">
            <form>

                <input type="submit"
                       style="background:url('/statics/images/sousuo2x.png') no-repeat;background-size: 70%; width:28px; height:32px; border:0; padding:10px 0;margin-left: 5px;margin-top: 10px;"
                       value=""/>
                <input type="text" placeholder="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;请输入关键字" style="margin-left: 16px;"
                       name="name">
            </form>
            <br/><br/><span style="font-size: 10px;margin-left: 80%;">&nbsp;&nbsp;*支持模糊查询</span>
            <br/>
            <%--<div style="margin-top: 24px;text-align: center;height: 20px;width: 200px;"><span style="font-size: 10px;margin-right: -50px;">&nbsp;&nbsp;*支持模糊查询</span></div>
            <div style="margin-top: -30px;margin-right: 250px;height: 50px;width: 50px;">
                   <a href="/pages/insertInfo.jsp" style="text-decoration: none; background: 1F1F1F;width: 50px;height: 30px;margin-left: -146%;margin-top:-15%">新增</a>
                    </div>
                </div>--%>
            <%--    <input type="submit" value="search" style="box-sizing: content-box"/>--%>
        </div>
        <br/>
        <core:forEach items="${feedbackInfoList}" var="flist">
            <div style="height: 60px">
                <span style="color: blue;font-size: 18px">
                    <a id="axs" href="<%=basePath%>ErRoR_MeT?name=${flist.id}"
                       target="_blank">${flist.issue}</a>
                </span>
                    <%-- <div><a style="color: green">${flist.feedbackPerson}</a></div>--%>
                <div>
                    <span style="color: gray">分类中&nbsp;&nbsp;&nbsp;&nbsp;${flist.feedbackDate}</span></div>
            </div>
        </core:forEach>
    </form>
</div>
</body>
</html>