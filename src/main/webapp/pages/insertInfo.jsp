<%@ page language="java" import="java.util.*" pageEncoding="UTF-8" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD//XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<html>
<head>
    <title>世腾-问题列表</title>
    <script type="text/javascript">
        function b1vf3hk() {
            var form = document.forms[0];
            // form.data = {name: name};
            // form.type = JSON;
            form.action = "<%=basePath%>fileUpload";
            form.method = "post";
            form.enctype="multipart/form-data";
            form.submit();
        }
    </script>
    <style type="text/css">
        #circle input {
            width: 300px;
            height: 25px;
            margin-bottom: 10px;
            margin-top: 10px;
        }

        .button-21 {
            height: 30px;
            width: 80px;
        }

        #circle {
            font-family: "Segoe UI";
        }

        #circle label {
            color: #575657;
        }

        td {
            /*border: #023B3C 1px solid;*/
        }

        tr {
            width: 300px;
        }
    </style>
</head>
<%@ taglib prefix="core" uri="http://java.sun.com/jsp/jstl/core" %>
<body style="background-color: #d2d7dd;">
<div style="height: 300px;width: 100%;text-align: center;">
    <form>
        <div style="text-align: right;width: 100%;" id="circle">
            <%--            enctype="multipart/form-data"--%>
            <form action="fileUpload" method="post" enctype="multipart/form-data">
                <label style="text-align: center"><h2>问题录入</h2></label>
                <table style="/*border: #023B3C 1px solid;*/width: 50%;text-align: right;margin-left: 300px;">
                    <tr>
                        <td rowspan="4">
                            <div style="text-align: left">问题点：</div>
                            <br/>
                            <label>
                                <textarea rows="9" cols="40" placeholder="问题点" name="filed_5"></textarea></label>
                        </td>
                        <td>
                            <label>反馈日期：<input type="date" placeholder="0000-00-00" name="filed_1"></label>
                        </td>
                    </tr>
                    <tr>
                        <td><label>反馈公司：<input type="text" placeholder="反馈公司" name="filed_2"></label></td>
                    </tr>
                    <tr>
                        <td><label>反馈人：<input type="text" placeholder="反馈人" name="filed_3"></label></td>
                    </tr>
                    <tr>
                        <td>
                            <label>问题分类：
                                <select name="filed_7" style="width: 307px">
                                    <option value="销售采购">销售采购</option>
                                    <option value="任务">任务</option>
                                    <option value="操作问题">操作问题</option>
                                    <option value="程序bug">程序bug</option>
                                </select>
                            </label>
                        </td>
                    </tr>
                    <tr>
                        <td rowspan="4">
                            <div style="text-align: left">备注：</div>
                            <br/>
                            <label>
                                <textarea rows="9" cols="40" placeholder="备注" name="filed_9"></textarea></label>
                        </td>
                        <td>
                            <label>产生原因：<input type="text" placeholder="产生原因" name="filed_4"></label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>解决方式：<input type="text" placeholder="解决方式" name="filed_6"></label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>处理人：<input type="text" placeholder="处理人" name="filed_8"></label><br/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>backup:<input type="file" value="unload" name="file" id="file" accept="*"
                                                 enctype="multipart/form-data"/></label>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2"><label style="margin-right: 30%;width: 100%">
<%--                            <input type="submit" value="提交"/>--%>
                            <button class="button-21" type="button" onclick="b1vf3hk()">添加</button>
                        </label></td>
                    </tr>
                </table>
            </form>
        </div>
    </form>
</div>
</body>
</html>