<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD//XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<html>
<head>
    <title>世腾-问题详情</title>
</head>
<%--<%=request.getParameter("name")%>--%>
<%@ taglib prefix="_Ore" uri="http://java.sun.com/jsp/jstl/core" %>
<body>
<%--<_Ore:forEach items="${error_l}" var="E_de">--%>
    <div style="text-align: center">
        <h2>${error_l[0].solution}</h2>
        <div>
            <span>${error_l[0].feedbackDate}</span>
            <span>${error_l[0].feedbackCompany}</span>
        </div>
        <div style="font-size: 20px">${error_l[0].issue}</div>
    </div>
<%--</_Ore:forEach>--%>
</body>
</html>