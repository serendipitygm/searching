<%@ page language="java" import="java.util.*" pageEncoding="UTF-8" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD//XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<html>
<head>
    <title>世腾-新增问题</title>
</head>
<%@ taglib prefix="core" uri="http://java.sun.com/jsp/jstl/core" %>
<body>
<%--<_Ore:forEach items="${error_l}" var="E_de">--%>
<div style="text-align: center">
    <form action="/saveInfo">
        <table>
            <tr>
                <td>feedbackDate:</td>
                <td><input name="feedbackDate" type="text"/></td>
            </tr>
            <tr>
                <td>feedbackCompany:</td>
                <td><input name="feedbackCompany" type="text"/></td>
            </tr>
            <tr>
                <td>feedbackPerson:</td>
                <td><input name="feedbackPerson" type="text"/></td>
            </tr>
            <tr>
                <td>issue:</td>
                <td><input name="issue" type="text"/></td>
            </tr>
            <tr>
                <td>solution:</td>
                <td><input name="solution" type="text"/></td>
            </tr>
            <tr>
                <td>issueCategory:</td>
                <td><input name="issueCategory" type="text"/></td>
            </tr>
            <tr>
                <td>remarks:</td>
                <td><input name="remarks" type="text"/></td>
            </tr>
            <tr>
                <td>processor:</td>
                <td><input name="processor" type="text"/></td>
            </tr>
            <tr>
                <td><input type="button" value="submit"/></td>
            </tr>
        </table>
    </form>
</div>
<%--</_Ore:forEach>--%>
</body>
</html>