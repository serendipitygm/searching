<%@ page language="java" import="java.util.*" pageEncoding="UTF-8" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD//XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<html>
<head>
    <title>世腾-问题列表</title>
</head>
<%@ taglib prefix="core" uri="http://java.sun.com/jsp/jstl/core" %>
<body>
<form action="/queryInformation">
<div style="text-align: center">
    <a href="/pages/insertInfo.jsp">New</a>
    <input type="text" name="name" id="name"/>
    <input type="submit" value="search"/>
</div>
<core:forEach items="${feedbackInfoList}" var="flist">
    <div style="border: darkgray 2px solid">
        <h3 style="color: blue">
            <a href="<%=basePath%>/ErRoR_MeT?name=${flist.id}"
               target="_blank">${flist.solution}</a>
        </h3>
        <div><span style="color: gray">${flist.feedbackDate}</span>${flist.issue}</div>
        <div><a style="color: green">${flist.feedbackPerson}</a></div>
    </div>
</core:forEach>
</form>
</body>
</html>